//
//  RootViewModel.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 05/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit

class RootViewModel {
    
    lazy var categoriesRequestManager = CategoriesRequestManager()
    weak var delegateDataReciver: RootViewDataReciever?
    
    func fetchDataFromServer() {
        categoriesRequestManager.getCategoriesServiceResults { (categoriesData, error) in
            if error == nil {
                self.saveCategoriesDataToDatabse(categriesResponse: categoriesData)
                self.delegateDataReciver?.dataRecieved(categoriesData: categoriesData)
            }
        }
    }
    
    func saveCategoriesDataToDatabse(categriesResponse: CategoriesDataResponseModel?) {
        guard let categories = categriesResponse?.categories else { return }
        guard let rankings =  categriesResponse?.rankings else { return }
        
        // Prepare Realm Data Object for saving.
        let categoriesStorage = CategoriesStorage()
        categoriesStorage.id = 101
        
        // Prepare Categories Array
        for category in categories {
            let categoryStorage = CategoryStorage()
            
            guard let childCategories = category.childCategories else { continue }
            for childCategory in childCategories {
                categoryStorage.childCategories.append(childCategory)
            }
            categoryStorage.id = category.id ?? -1
            categoryStorage.name = category.name ?? ""
            
            guard let products = category.products else { continue }
            for product in products {
                let productStorage = CategoryProductStorage()
                productStorage.dateAdded = product.dateAdded ?? ""
                productStorage.id = product.id ?? -1
                productStorage.name = product.name ?? ""
                
                let tax = TaxStorage()
                tax.name = (product.tax?.name ?? .vat == .vat) ? "VAT" : "VAT4"
                tax.value = product.tax?.value ?? 0.0
                productStorage.tax = tax
                
                guard let variants = product.variants else { continue }
                for variant in variants {
                    let variantSt = VariantStorage()
                    variantSt.color = variant.color ?? ""
                    variantSt.price = variant.price ?? 0
                    variantSt.id = variant.id ?? 0
                    variantSt.size = variant.size ?? 0
                    productStorage.variants.append(variantSt)
                }
                categoryStorage.products.append(productStorage)
            }
            categoriesStorage.categories.append(categoryStorage)
        }
        
        // Prepare Ranking Array
        var itemId = 0
        for (index, ranking) in rankings.enumerated() {
            let rankStorage = RankingStorage()
            rankStorage.ranking = ranking.ranking ?? ""
            rankStorage.id = index
            guard let rankProducts = ranking.products else { continue }
            for rankProduct in rankProducts {
                let productStorage = RankingProductStorage()
                productStorage.id = rankProduct.id ?? 0
                productStorage.orderCount = rankProduct.orderCount ?? 0
                productStorage.shares = rankProduct.shares ?? 0
                productStorage.viewCount = rankProduct.viewCount ?? 0
                productStorage.storageId = rankStorage.id
                productStorage.itemId = itemId
                rankStorage.products.append(productStorage)
                itemId = itemId + 1
            }
            categoriesStorage.rankings.append(rankStorage)
        }
        
        DatabaseUtility.shared.saveCategories(categoriesStorage)
    }
}

protocol RootViewDataReciever: class {
    func dataRecieved(categoriesData: CategoriesDataResponseModel?)
}

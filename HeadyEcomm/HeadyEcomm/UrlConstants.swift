//
//  UrlConstants.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 04/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import Foundation

class URLConstants {
    
    public static let categoriesDataURL = "https://stark-spire-93433.herokuapp.com/json"
}

//
//  ViewController.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 03/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let viewModel = RootViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        viewModel.delegateDataReciver = self
        viewModel.fetchDataFromServer()
    }
    
    func setupHomeView(categoriesData: CategoriesDataResponseModel) {
        let homeViewController: HomeViewController = UIStoryboard(name: "HomeView", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        homeViewController.viewModel = HomeViewModel()
        let navigationController = UINavigationController(rootViewController: homeViewController)
        self.view.addSubview(navigationController.view)
        addChild(navigationController)
        navigationController.didMove(toParent: self)
    }
}

extension ViewController: RootViewDataReciever {
    func dataRecieved(categoriesData: CategoriesDataResponseModel?) {
        if let categories = categoriesData {
            self.setupHomeView(categoriesData: categories)
        }
    }
}

//
//  RankingsViewModel.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 07/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit
import RealmSwift

class RankingsViewModel {
    var rankings: [RankingStorage]?
    let realm = try! Realm()
    
    func getProductStorage(forIndex: Int) -> [CategoryProductStorage] {
        var productsStorage = [CategoryProductStorage]()
        let ranking = rankings?[forIndex]
        guard let rankProducts = ranking?.products.filter("storageId = %d", ranking?.id ?? 0) else { return productsStorage }
        for rankProduct in rankProducts {
            let product = DatabaseUtility.shared.getProduct(id: rankProduct.id)
            if let product = product.first {
                try! realm.write {
                    if rankProduct.viewCount > 0 {
                        product.rankAttributedKey = "View Count"
                        product.rankAttributedValue = rankProduct.viewCount
                    } else if rankProduct.shares > 0 {
                        product.rankAttributedKey = "Share Count"
                        product.rankAttributedValue = rankProduct.shares
                    } else if rankProduct.orderCount > 0 {
                        product.rankAttributedKey = "Order Count"
                        product.rankAttributedValue = rankProduct.orderCount
                    }
                }
                productsStorage.append(product)
            }
        }
        return productsStorage
    }
}

//
//  CaegoryCollectionViewModel.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 07/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit

class CategoriesCollectionViewModel: SectionableViewModel {
    var categories: [CategoryStorage]?
    
    func hasDataToBeShown() -> Bool {
        return categories?.count ?? 0 > 0
    }
    
    func getCategoryStorage(forIndex: Int) -> [CategoryStorage] {
        var categoryStroage = [CategoryStorage]()
        let category = categories?[forIndex]
        guard let childCategories = category?.childCategories else { return categoryStroage }
        for childCategoryId in childCategories {
            let category = DatabaseUtility.shared.getCategory(id: childCategoryId)
            if let category = category.first {
                categoryStroage.append(category)
            }
        }
        return categoryStroage
    }
}

//
//  ProductListViewModel.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 07/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit

class ProductListViewModel {
    var products: [CategoryProductStorage]?
    var shouldShowRankDetails: Bool = false
}

class ProductListCollectionViewModel {
    var productName: String?
    var rankDetail: String?
    var shouldShowRankDetails: Bool = false
    var productStorage: CategoryProductStorage? {
        didSet {
            setupViewModel()
        }
    }
    
    func setupViewModel() {
        productName = productStorage?.name
        if productStorage?.rankAttributedKey.count ?? 0 > 0 {
            rankDetail = String(format: "%@ (%d)", productStorage?.rankAttributedKey ?? "", productStorage?.rankAttributedValue ?? 0)
        }
    }
}

//
//  HomeViewModel.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 04/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit

protocol SectionableViewControllerDelegate: class {
    func controllerUIUpdated(viewController: UIViewController)
}

protocol SectionableViewController {
    var delegate: SectionableViewControllerDelegate? {get set}
    
    func getHeightOfController() -> CGFloat
    func updateContent()
}

// All the ViewModel Type
protocol SectionableViewModel: class {
    func hasDataToBeShown() -> Bool
}

enum HomvViewSectionSequance: String {
    case categories
    case rankings
}

enum HomeViewSectionType: String {
    case categoriesSection
    case rankingsSection
    
    static func getSequanceOfSection(csv: String) -> [HomeViewSectionType] {
        let sectionsStrings = csv.components(separatedBy: ",")
        var sections = [HomeViewSectionType]()
        
        for sectionString in sectionsStrings {
            let section = sectionString.lowercased()
            switch section {
            case HomvViewSectionSequance.categories.rawValue.lowercased():
                sections.append(.categoriesSection)
            case HomvViewSectionSequance.rankings.rawValue.lowercased():
                sections.append(.rankingsSection)
            default:
                break
            }
        }
        
        return sections
    }
}

class HomeViewModel {
 
    var categories: [CategoryStorage]?
    var rankings: [RankingStorage]?
    
    let sequanceOfHomeViewSection = String(format: "%@,%@", HomvViewSectionSequance.categories.rawValue, HomvViewSectionSequance.rankings.rawValue)
    
    lazy var homeViewSections: [HomeViewSectionType] = {
        return HomeViewSectionType.getSequanceOfSection(csv: sequanceOfHomeViewSection)
    }()
    
    // ViewModels
    lazy var categoryCollectionViewModel = CategoriesCollectionViewModel()
    lazy var rankingsViewModel = RankingsViewModel()
    
    func setupViewModel() {
        let _ = getRootCategories()
    }
    
    init() {
        self.rankings = DatabaseUtility.shared.getRankings()
        self.categories = DatabaseUtility.shared.getCategories()
        setupViewModel()
    }
    
    func getRootCategories() -> [CategoryStorage]? {
        var rootCategories = [CategoryStorage]()
        guard let origanalCategories = categories else { return rootCategories }
        
        var pureCategoriesWithoutProduct = origanalCategories.filter { $0.products.count == 0 }
        rootCategories = origanalCategories.filter { $0.products.count != 0 }
        
        for category in pureCategoriesWithoutProduct {
            let childCategories = category.childCategories
            pureCategoriesWithoutProduct = pureCategoriesWithoutProduct.filter { !childCategories.contains($0.id)}
        }
        
        pureCategoriesWithoutProduct.forEach { rootCategories.append($0) }
        rootCategories.forEach { print($0.id) }
        
        return rootCategories
    }
}


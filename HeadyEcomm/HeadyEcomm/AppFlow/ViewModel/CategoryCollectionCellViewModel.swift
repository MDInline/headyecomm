//
//  CategoryCollectionCellViewModel.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 07/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit

class CategoryCollectionCellViewModel {
    var categoryName: String?
    var isPureCategory: Bool = true
    
    var category: CategoryStorage? {
        didSet {
            setupViewModel()
        }
    }
    
    func setupViewModel() {
        categoryName = category?.name
        if category?.products.count ?? 0 > 0 {
            isPureCategory = false
        }
    }
}

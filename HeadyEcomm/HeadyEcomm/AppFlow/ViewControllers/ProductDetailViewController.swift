//
//  ProductDetailViewController.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 08/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit

class ProductDetailViewController: BaseViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var sizeView: UIView!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    var viewModel: ProductDetailViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        updateContentData()
        productImage.layer.borderColor = UIColor.lightGray.cgColor
        productImage.layer.borderWidth = 1.0
    }

    func updateContentData() {
        priceLabel.text = viewModel?.productPrice
        productTitle.text = viewModel?.productTitle
        sizeLabel.text = viewModel?.productSizeVariants
        colorLabel.text = viewModel?.productColorVariants
        
        if !(viewModel?.shouldShowSizeVariants ?? false) {
            sizeView.isHidden = true
        }
        
        if !(viewModel?.shouldShowColorVariants ?? false) {
            sizeView.isHidden = true
        }
    }

    @objc override func cartIconTapped() {
        super.cartIconTapped()
    }
}

class ProductDetailViewModel {
    var productTitle: String?
    var productPrice: String?
    var productColorVariants: String?
    var productSizeVariants: String?
    var shouldShowColorVariants: Bool = true
    var shouldShowSizeVariants: Bool = true
    
    var productStorage: CategoryProductStorage? {
        didSet {
            setupViewModel()
        }
    }
    
    func setupViewModel() {
    
        productTitle = productStorage?.name
        productPrice = String(format: "Rs %d/-", productStorage?.variants.first?.price ?? 0)
        productColorVariants = "Color: "
        productSizeVariants = "Size: "
        guard let variants = productStorage?.variants else { return }
        for variant in variants {
            if variant.color == "null" {
                shouldShowColorVariants = false
            } else {
                productColorVariants = String(format: "%@ %@", productColorVariants ?? "", variant.color)
            }
            
            if variant.size == 0 {
                shouldShowSizeVariants = false
            } else {
                productSizeVariants = String(format: "%@ %d", productSizeVariants ?? 0, variant.size)
            }
        }
    }
}

//
//  RankingsViewController.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 07/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit

class RankingsViewController: UIViewController {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var viewModel: RankingsViewModel?
    var delegate: SectionableViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var rect = self.view.frame
        rect.size.width = screenSize.width
        self.view.frame = rect
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.delegate?.controllerUIUpdated(viewController: self)
        }
    }

    func showProductList(forIndex: Int) {
        let storyboard = UIStoryboard.init(name: "HomeView", bundle: nil)
        let productListVC = storyboard.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        let products = viewModel?.getProductStorage(forIndex: forIndex)
        let productListViewModel = ProductListViewModel()
        productListViewModel.products = products
        productListViewModel.shouldShowRankDetails = true
        productListVC.viewModel = productListViewModel
        self.navigationController?.pushViewController(productListVC, animated: true)

    }
}

extension RankingsViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.rankings?.count ?? 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as! CategoryCollectionViewCell

        let ranking = viewModel?.rankings?[indexPath.row]
        cell.categoryTitle.text = ranking?.ranking
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showProductList(forIndex: indexPath.row)
    }
}

extension RankingsViewController: SectionableViewController {
    func getHeightOfController() -> CGFloat {
        return collectionView.contentSize.height + leftRightMargin + cellSpacing + 25.0 // TopViewHeight
    }
    
    func updateContent() {
        
    }
}

extension RankingsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (contentView.frame.size.width - (leftRightMargin + 30)) / 2
        return CGSize(width: width, height: width)
    }
}

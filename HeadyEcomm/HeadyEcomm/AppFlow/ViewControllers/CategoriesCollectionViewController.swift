//
//  CategoriesCollectionViewController.swift
//  HeadyEcomm
//
//  Created by Mandeep on 05/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit

let screenSize = UIScreen.main.bounds.size
let leftRightMargin: CGFloat = 20.0
let cellSpacing: CGFloat = 10.0

class CategoriesCollectionViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    weak var delegate: SectionableViewControllerDelegate?
    var viewModel: CategoriesCollectionViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         collectionView.reloadData()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var rect = self.view.frame
        rect.size.width = screenSize.width
        self.view.frame = rect
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.delegate?.controllerUIUpdated(viewController: self)
        }
    }
    
    func showProductList(forIndex: Int) {
        let storyboard = UIStoryboard.init(name: "HomeView", bundle: nil)
        let productListVC = storyboard.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        if let category = viewModel?.categories?[forIndex] {
            let products = category.products
            let productListViewModel = ProductListViewModel()
            productListViewModel.products = Array(products)
            productListVC.viewModel = productListViewModel
        }
        self.navigationController?.pushViewController(productListVC, animated: true)
    }
    
    func showCategoriesList(forIndex: Int) {
        let storyboard = UIStoryboard.init(name: "HomeView", bundle: nil)
        let categoryVC = storyboard.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        
        let categoryViewModel = CategoryViewModel()
        categoryViewModel.categories = viewModel?.getCategoryStorage(forIndex: forIndex)
        categoryVC.viewModel = categoryViewModel
        self.navigationController?.pushViewController(categoryVC, animated: true)
    }
}

extension CategoriesCollectionViewController: SectionableViewController {
    func getHeightOfController() -> CGFloat {
        return collectionView.contentSize.height + leftRightMargin
    }
    
    func updateContent() {
        
    }
}

extension CategoriesCollectionViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.categories?.count ?? 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as! CategoryCollectionViewCell
        
        let cellViewModel = CategoryCollectionCellViewModel()
        cellViewModel.category = viewModel?.categories?[indexPath.row]
        cell.viewModel = cellViewModel
        cell.updateData()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? CategoryCollectionViewCell
        print(indexPath.row)
        if cell?.viewModel?.isPureCategory ?? false {
            showCategoriesList(forIndex: indexPath.row)
        } else {
            // show Product List
            showProductList(forIndex: indexPath.row)
        }
    }
}

extension CategoriesCollectionViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (contentView.frame.size.width - leftRightMargin) / 2
        return CGSize(width: width, height: width)
    }

}

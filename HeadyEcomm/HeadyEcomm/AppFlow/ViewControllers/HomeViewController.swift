//
//  HomeViewController.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 04/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit
/******* App Flow
 Home-> Categories-Ranking
 Categories->Sub Categories
 Sub Categories->Products
 Ranking-> Selected Group Products
*******/
extension UIColor {
    static let navigationBarColor = UIColor.systemBlue
}

extension UIViewController {
    open override func awakeFromNib() {
        

    }
}

class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        let cartIcon = UIBarButtonItem(image: UIImage(named: "cart_icon"), style: .plain, target: self, action: Selector(("cartIconTapped")))
        self.navigationItem.rightBarButtonItem  = cartIcon
    }
    
    func cartIconTapped() {
        
    }
}

class HomeViewController: BaseViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    
    var viewModel: HomeViewModel?
    var categoryCollectionViewController: CategoriesCollectionViewController?
    var rankingViewController: RankingsViewController?
    var categoryCollectionView: UIView?
    var rankingView: UIView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupControls()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var rect = self.view.frame
        rect.size.width = screenSize.width
        self.view.frame = rect
    }
    
    func setupControls() {
        // Navigation Only
        self.navigationItem.title = "Heady Ecomm";
        self.navigationController?.navigationBar.barTintColor = UIColor.navigationBarColor
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isHidden = false
        let menuIcon = UIBarButtonItem(image: UIImage(named: "menu_icon"), style: .plain, target: self, action: Selector(("menuTapped")))
        self.navigationItem.leftBarButtonItem  = menuIcon
        initalizeSections()
    }
    
    @objc func menuTapped() {
        
    }
    
    @objc override func cartIconTapped() {
        super.cartIconTapped()
    }
    
    func initalizeSections() {
        // Setup code for initalize section
        setupCategoriesSectionView()
        setupRankingSectionViews()
    }
    
    func invalidateSections() {
        // invalidateSections
    }
    
    func setupCategoriesSectionView() {
        if categoryCollectionViewController == nil {
            let storyboard = UIStoryboard.init(name: "HomeView", bundle: nil)
            let collectionVC = storyboard.instantiateViewController(withIdentifier: "CategoriesCollectionViewController") as! CategoriesCollectionViewController
            categoryCollectionViewController = collectionVC
            categoryCollectionViewController?.delegate = self
            categoryCollectionViewController?.viewModel = viewModel?.categoryCollectionViewModel
            categoryCollectionViewController?.viewModel?.categories = viewModel?.getRootCategories()
            categoryCollectionView = collectionVC.view
            
            addChild(collectionVC)
            didMove(toParent: collectionVC)
            scrollView.addSubview(collectionVC.view)
        }
    }
    
    func setupRankingSectionViews() {
        if rankingViewController == nil {
            let storyboard = UIStoryboard.init(name: "HomeView", bundle: nil)
            let rankingVC = storyboard.instantiateViewController(withIdentifier: "RankingsViewController") as! RankingsViewController
            rankingViewController = rankingVC
            rankingViewController?.delegate = self
            rankingViewController?.viewModel = viewModel?.rankingsViewModel
            rankingViewController?.viewModel?.rankings = viewModel?.rankings
            rankingView = rankingVC.view
            
            addChild(rankingVC)
            didMove(toParent: rankingVC)
            scrollView.addSubview(rankingVC.view)
        }
    }
    
    func adjustFrameForSections() {
        guard let sections = viewModel?.homeViewSections else { return }

        let margin: CGFloat = 0.0
        var yPos: CGFloat = 0.0
        let width = screenSize.width - margin * 2

        for section in sections {
            guard let view = getView(section: section) else {
                continue
            }
            let controller = getViewController(section: section)
            let height = controller?.getHeightOfController() ?? 0.0
            let frame = CGRect(x: margin, y: yPos, width: width, height: height)
            view.frame = frame
            yPos = frame.maxY
            yPos += 1.0 // Margin between views.
        }
        scrollView.contentSize = CGSize(width: width, height: yPos)
        
    }
    
    func getViewController(section: HomeViewSectionType) -> SectionableViewController? {
        switch section {
        case .categoriesSection:
            return categoryCollectionViewController
        case .rankingsSection:
            return rankingViewController
        }
    }
    
    func getView(section: HomeViewSectionType) -> UIView? {
        switch section {
        case .categoriesSection:
            return categoryCollectionView
        case .rankingsSection:
            return rankingView
        }
    }
    
}

extension HomeViewController: SectionableViewControllerDelegate {
    func controllerUIUpdated(viewController: UIViewController) {
        // Update Home Controller UI
        adjustFrameForSections()
    }
}

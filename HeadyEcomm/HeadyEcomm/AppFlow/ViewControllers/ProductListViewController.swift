//
//  ProductListViewController.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 07/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit

class ProductListViewController: BaseViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var viewModel: ProductListViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var rect = self.view.frame
        rect.size.width = screenSize.width
        self.view.frame = rect
    }
    
    @objc override func cartIconTapped() {
        super.cartIconTapped()
    }
    
    func showProductDetail(forIndex: Int) {
        let storyboard = UIStoryboard.init(name: "HomeView", bundle: nil)
        let productVC = storyboard.instantiateViewController(withIdentifier: "ProductDetailViewController") as! ProductDetailViewController
        let products = viewModel?.products?[forIndex]
        let productViewModel = ProductDetailViewModel()
        productViewModel.productStorage = products
        productVC.viewModel = productViewModel
        self.navigationController?.pushViewController(productVC, animated: true)
    }
}

extension ProductListViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.products?.count ?? 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCollectionViewCell", for: indexPath) as! ProductListCollectionViewCell
        
        let cellViewModel = ProductListCollectionViewModel()
        cellViewModel.productStorage = viewModel?.products?[indexPath.row]
        cellViewModel.shouldShowRankDetails = viewModel?.shouldShowRankDetails ?? false
        cell.viewModel = cellViewModel
        cell.updateData()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        showProductDetail(forIndex: indexPath.row)
    }
}

extension ProductListViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (screenSize.width - 64) / 2
        return CGSize(width: width, height: width)
    }

}

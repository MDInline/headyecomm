//
//  CategoryViewController.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 07/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit

class CategoryViewController: BaseViewController {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!

    var viewModel: CategoryViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var rect = self.view.frame
        rect.size.width = screenSize.width
        self.view.frame = rect
    }
    
    @objc override func cartIconTapped() {
        super.cartIconTapped()
    }
}

extension CategoryViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.categories?.count ?? 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionViewCell", for: indexPath) as! CategoryCollectionViewCell
        
        let cellViewModel = CategoryCollectionCellViewModel()
        cellViewModel.category = viewModel?.categories?[indexPath.row]
        cell.viewModel = cellViewModel
        cell.updateData()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let cell = collectionView.cellForItem(at: indexPath) as? CategoryCollectionViewCell
        print(indexPath.row)
    }
}

extension CategoryViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (screenSize.width - 64) / 2
        return CGSize(width: width, height: width)
    }

}

class CategoryViewModel {
    var categories: [CategoryStorage]?
}

//
//  ProductListCollectionViewCell.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 07/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit

class ProductListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var rankTitle: UILabel!
    @IBOutlet weak var rankViewHeight: NSLayoutConstraint!
    
    var viewModel: ProductListCollectionViewModel?
    
    override func awakeFromNib() {
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func updateData() {
        productImage.image = UIImage(named: "categoryProduct")
        productTitle.text = viewModel?.productName
        
        if viewModel?.shouldShowRankDetails ?? false {
            rankTitle.text = viewModel?.rankDetail
            rankViewHeight.constant = 25.0
        } else {
            rankViewHeight.constant = 0.0
        }
    }
}

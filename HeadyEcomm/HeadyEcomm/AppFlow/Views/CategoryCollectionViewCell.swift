//
//  CategoryCollectionViewCell.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 07/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    
    var viewModel: CategoryCollectionCellViewModel?
    
    override func awakeFromNib() {
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func updateData() {
        categoryImage.image = viewModel?.isPureCategory ?? false ? UIImage(named: "categoryPure") : UIImage(named: "categoryProduct")
        categoryTitle.text = viewModel?.categoryName
    }
}

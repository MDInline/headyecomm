//
//  CategoriesDataService.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 04/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import Nikka

class CategoriesDataServices: NSObject {

    public func getCategoriesData(requestModel: CategoriesDataRequestModel, completion:@escaping(CategoriesDataResponseModel) -> Void, error:@escaping (String, Int?) -> Void) {
        
        let httpProvider: CategoriesHttpProvider = CategoriesHttpProvider()
        if let route = try? Route.categoriesRequest(requestModel) {
            httpProvider.request(route).responseObject { (response: Response<CategoriesDataResponseModel>) in
                if let value: CategoriesDataResponseModel = response.result.value {
                    completion(value)
                } else {
                    error("Error in parsing", jsonParsingFailed)
                }
            }
        }
    }
}

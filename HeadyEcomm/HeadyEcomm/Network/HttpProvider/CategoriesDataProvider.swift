//
//  CategoriesDataProvider.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 04/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import Nikka

class CategoriesHttpProvider: HTTPProvider {
    var baseURL = URL(string: URLConstants.categoriesDataURL)!
}

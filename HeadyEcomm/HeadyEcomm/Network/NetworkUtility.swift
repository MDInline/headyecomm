//
//  NetworkUtility.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 04/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import Foundation

class NetworkUtility: NSObject {
    public static func getObjectAsRequestDictionary<T: Encodable> (obj: T) throws -> [String: Any]? {
        let jsonData = try JSONEncoder().encode(obj)
        let dictionary = try JSONSerialization.jsonObject(with: jsonData, options: .mutableLeaves)
        return dictionary as? [String: Any]
    }
}

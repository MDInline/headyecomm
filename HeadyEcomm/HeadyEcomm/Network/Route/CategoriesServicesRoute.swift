//
//  CategoriesServicesRoute.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 04/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import Foundation

import Nikka

enum RunTimeError: Error {
    case runTimeError(String)
}

extension Route {
    static let categoriesRequest = { (categoriesRequest: CategoriesDataRequestModel) throws -> Route  in
        let dictionary = try NetworkUtility.getObjectAsRequestDictionary(obj: categoriesRequest)
        if let request = dictionary {
            return Route(path: "", method: .get, params: request)
        }
        throw RunTimeError.runTimeError("Request is not valid for raw get CategoriesDataRequestModel")
    }

}

//
//  CategoriesRequestManager.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 04/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import Foundation

let defaultErrorCode = 1001
let jsonParsingFailed = 1002

class CategoriesRequestManager: NSObject {
    
    lazy var service = CategoriesDataServices()
    
    func getCategoriesServiceResults(completion:@escaping(_ result: CategoriesDataResponseModel?, _ error: Error?) -> Void) {
        let requestModel = CategoriesDataRequestModel()
        service.getCategoriesData(requestModel: requestModel, completion: { result in
            completion(result, nil)
        }) { (strError, errorCode) in
            let error = NSError(domain: "", code: errorCode ?? defaultErrorCode, userInfo: [NSLocalizedDescriptionKey: strError]) as Error
            completion(nil, error)
        }
    }
}

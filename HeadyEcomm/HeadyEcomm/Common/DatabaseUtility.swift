//
//  DatabaseUtility.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 06/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import Foundation
import RealmSwift

class DatabaseUtility: NSObject {
    
    static var shared: DatabaseUtility = {
        let dbUtility = DatabaseUtility()
        return dbUtility
    }()

    func checkIfMigrationNeeded() {
        let configCheck = Realm.Configuration();
        var schemaVersion: UInt64 = 0
        do {
            schemaVersion = try schemaVersionAtURL(configCheck.fileURL!)
            let configuration = Realm.Configuration (
                schemaVersion: schemaVersion,
                migrationBlock: { migration, oldSchemaVersion in
                    if oldSchemaVersion < schemaVersion {
                        print("Current Database Scheme : \(schemaVersion)")
                    }
                }
            )
            Realm.Configuration.defaultConfiguration = configuration
        } catch  {
            print(error)
        }
    }
    
    func saveCategories(_ catagoryStorage: CategoriesStorage) {
        let realm = try! Realm()
        try! realm.write {
            realm.add(catagoryStorage, update: Realm.UpdatePolicy.all)
        }
    }
    
    func getCategories() -> [CategoryStorage] {
        var values = [CategoryStorage]()
        DispatchQueue(label: "background").sync{
            autoreleasepool {
                if let realm = try? Realm() {
                    guard let categories = realm.objects(CategoriesStorage.self).first else { return }
                    values = Array(categories.categories)
                }
            }
        }
        
        return values
    }
    
    func getRankings() -> [RankingStorage] {
        var values = [RankingStorage]()
        DispatchQueue(label: "background").sync{
            autoreleasepool {
                if let realm = try? Realm() {
                    let rankings = realm.objects(RankingStorage.self)
                    values = Array(rankings)
                }
            }
        }
        
        return values
    }
    
    func getProduct(id: Int) -> [CategoryProductStorage] {
        var values = [CategoryProductStorage]()
        DispatchQueue(label: "background").sync{
            autoreleasepool {
                if let realm = try? Realm() {
                    let products = realm.objects(CategoryProductStorage.self).filter("id = %d", id)
                    values = Array(products)
                }
            }
        }
        
        return values
    }
    
    func getCategory(id: Int) -> [CategoryStorage] {
        var values = [CategoryStorage]()
        DispatchQueue(label: "background").sync{
            autoreleasepool {
                if let realm = try? Realm() {
                    let category = realm.objects(CategoryStorage.self).filter("id = %d", id)
                    values = Array(category)
                }
            }
        }
        
        return values
    }
}

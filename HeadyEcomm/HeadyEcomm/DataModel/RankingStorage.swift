//
//  RankingStorage.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 06/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import RealmSwift

class RankingStorage: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var ranking: String = ""
    let products = RealmSwift.List<RankingProductStorage>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

class RankingProductStorage: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var viewCount: Int = 0
    @objc dynamic var orderCount: Int = 0
    @objc dynamic var shares: Int = 0
    @objc dynamic var storageId: Int = 0
    @objc dynamic var itemId: Int = 0
    
    override class func primaryKey() -> String? {
        return "itemId"
    }
}

class RankedProduct: NSObject {
    var name: String?
    var products: [CategoryProductStorage]?
}

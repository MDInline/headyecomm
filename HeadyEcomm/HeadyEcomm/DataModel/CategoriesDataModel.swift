//
//  CategoriesDataModel.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 04/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import Foundation

class CategoriesDataRequestModel: NSObject, Codable {
    
}

// MARK: - CategoriesItem
@objcMembers class CategoriesDataResponseModel: NSObject, Codable {
    let categories: [Category]?
    let rankings: [Ranking]?
    
    init(categories: [Category]?, rankings: [Ranking]?) {
        self.categories = categories
        self.rankings = rankings
    }
}

// MARK: - Category
@objcMembers class Category: NSObject, Codable {
    let id: Int?
    let name: String?
    let products: [CategoryProduct]?
    let childCategories: [Int]?
    
    enum CodingKeys: String, CodingKey {
        case id, name, products
        case childCategories = "child_categories"
    }
    
    init(id: Int?, name: String?, products: [CategoryProduct]?, childCategories: [Int]?) {
        self.id = id
        self.name = name
        self.products = products
        self.childCategories = childCategories
    }
}

// MARK: - CategoryProduct
@objcMembers class CategoryProduct: NSObject, Codable {
    let id: Int?
    let name, dateAdded: String?
    let variants: [Variant]?
    let tax: Tax?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case dateAdded = "date_added"
        case variants, tax
    }
    
    init(id: Int?, name: String?, dateAdded: String?, variants: [Variant]?, tax: Tax?) {
        self.id = id
        self.name = name
        self.dateAdded = dateAdded
        self.variants = variants
        self.tax = tax
    }
}

// MARK: - Tax
@objcMembers class Tax: NSObject, Codable {
    let name: Name?
    let value: Double?
    
    init(name: Name?, value: Double?) {
        self.name = name
        self.value = value
    }
}

enum Name: String, Codable {
    case vat = "VAT"
    case vat4 = "VAT4"
}

// MARK: - Variant
@objcMembers class Variant: NSObject, Codable {
    let id: Int?
    let color: String?
    let size: Int?
    let price: Int?
    
    init(id: Int?, color: String?, size: Int?, price: Int?) {
        self.id = id
        self.color = color
        self.size = size
        self.price = price
    }
}

// MARK: - Ranking
@objcMembers class Ranking: NSObject, Codable {
    let ranking: String?
    let products: [RankingProduct]?
    
    init(ranking: String?, products: [RankingProduct]?) {
        self.ranking = ranking
        self.products = products
    }
}

// MARK: - RankingProduct
@objcMembers class RankingProduct: NSObject, Codable {
    let id, viewCount, orderCount, shares: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case viewCount = "view_count"
        case orderCount = "order_count"
        case shares
    }
    
    init(id: Int?, viewCount: Int?, orderCount: Int?, shares: Int?) {
        self.id = id
        self.viewCount = viewCount
        self.orderCount = orderCount
        self.shares = shares
    }
}

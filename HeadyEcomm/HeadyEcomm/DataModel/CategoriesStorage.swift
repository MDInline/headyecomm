//
//  CategoriesStorage.swift
//  HeadyEcomm
//
//  Created by Mandeep Dhiman on 06/07/20.
//  Copyright © 2020 Mandeep Dhiman. All rights reserved.
//

import RealmSwift
import Foundation

class CategoriesStorage: Object {
    @objc dynamic var id: Int = 0
    let categories = RealmSwift.List<CategoryStorage>()
    let rankings = RealmSwift.List<RankingStorage>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

class CategoryStorage: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    var products = RealmSwift.List<CategoryProductStorage>()
    var childCategories = RealmSwift.List<Int>()
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

class CategoryProductStorage: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var dateAdded: String = ""
    dynamic var variants = RealmSwift.List<VariantStorage>()
    @objc dynamic var tax: TaxStorage?
    @objc dynamic var rankAttributedKey: String = ""
    @objc dynamic var rankAttributedValue: Int = 0
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

class TaxStorage: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var value: Double = 0.0
}

class VariantStorage: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var color: String = ""
    @objc dynamic var size: Int = 0
    @objc dynamic var price: Int = 0
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
